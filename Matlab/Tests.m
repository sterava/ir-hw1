close all
clear variables %all
 
%Load MAP values from disk
load dataset

%Perform ANOVA
[~,~,stats] = anova1(AP, runID); %, 'off');
 
%Perform the multiple comparison test
multcompare(stats); %working like [~,~,h] = multcompare(stats);
 
%% Show the interactive Tukey's range test figure
set(gcf, 'Position', 0.7*get(0, 'Screensize'));
movegui(gcf,'center')
 
%... and save it to file
title("Tukey's range test")
xlabel('Average Precision')
print(gcf, '-dpng', 'AP Tukey.png');
 
 
%% Show the Average Precision boxplot
figure
set(gcf, 'Position', 0.7*get(0, 'Screensize'));
movegui(gcf,'center')
 
boxplot(AP(:,:), 'Labels', runID(:), 'Orientation', 'horizontal', 'Symbol', 'ro')
set(gca, 'YDir','reverse')
xlabel('Average Precision')
 
%... and save it to file
print(gcf, '-dpng', 'AP boxplot.png');